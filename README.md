﻿# microscopio-suelo

El proyecto tiene como objetivo el desarrollo/adaptación de un microscopio para monitoreo de microbiología en suelos y biofertilizantes.  
El desarrollo estará basado en el proyecto Openflexure (http://rwb27.github.io/openflexure  

Este proyecto será desarrollado en el marco de la residencia ReGOSH financiadas por CYTED que tendrán lugar en el CTA de Porto Alegre  
Aquí se puede consultar la propuesta https://docs.google.com/document/d/1D3V1CGe10bc6vhdXc39WuVj0bb5CqSzliB98pXYe53E/edit  

### modelo de repositorio para open hardware del proyecto https://github.com/FOSH-following-demand

Esta bueno tener en cuenta estas guía/recomendaciones a la hora de documentar el proyecto

This is a template repository. should be used as a guideline by people/groups developing equipment in this project

Readme can have a link to all the relevant files/documents and can/should be used to give examples of data collected with the device as well as the rational behind its construction.

Should also have mentions to code of conduct and contributing guidelines.
